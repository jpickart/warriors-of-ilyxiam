﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WarriorsOfIlyxiam {
    public class Map {
        public bool IsValid { get; set; }
        private List<List<string>> _map;

        public Map (string dataSource) {
            var path = "Map" + Path.DirectorySeparatorChar + dataSource;

            if (File.Exists(path) == false) {
                IsValid = false;
                return;
            }

            string[] mapData = File.ReadAllLines(path);
            _map = new List<List<string>>();

            foreach (var line in mapData) {
                _map.Add(new List<string>());

                for (int i = 0; i < line.Length; i++) {
                    _map[_map.Count -1].Add(line[i].ToString());
                }
            }

            IsValid = true;
        }
        
        // Removes the passed target from the game map
        public bool RemoveTarget(string target) {
            for (int row = 0; row < _map.Count; row++) {
                for (int col = 0; col <_map[row].Count; col++) {
                    if (target == _map[row][col]) {
                        _map[row][col] = Strings.Empty;
                        return true;
                    }
                }
            }

            // The target was not found
            return false;
        }

        // Gets the Coordinates for the passed target
        public Coordinates GetCoordinates(string target) {
            for (int row = 0; row < _map.Count; row++) {
                for (int col = 0; col <_map[row].Count; col++) {
                    if (target == _map[row][col]) {
                        return new Coordinates(row, col);
                    }
                }
            }

            return null;
        }

        internal int GetMessageRow() {
            return _map.Count;
        }

        internal int GetMessageCol() {
            return 0;
        }

        /// <summary>
        /// Checks if something is "near" the origin.
        /// "Near" is defined as immediately adjacent.
        /// </summary>
        public bool IsTargetNear(string origin, string target) {
            bool isNear = false;

            Coordinates coordinates = GetCoordinates(origin);

            if (coordinates != null) {
                if (_map[coordinates.Row][coordinates.Col + 1] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row][coordinates.Col - 1] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row + 1][coordinates.Col + 1] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row + 1][coordinates.Col] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row + 1][coordinates.Col - 1] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row - 1][coordinates.Col + 1] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row - 1][coordinates.Col] == target) {
                    isNear = true;
                }
                else if (_map[coordinates.Row - 1][coordinates.Col - 1] == target) {
                    isNear = true;
                }
            }

            return isNear;
        }
        
        public string GetNextTarget() {
            var row = -1;
            var col = -1;
            Coordinates coordinates = GetCoordinates(Strings.PlayerChar);

            if (coordinates != null) {
                row = coordinates.Row + 1;
                col = coordinates.Col;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row + 1;
                col = coordinates.Col + 1;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row;
                col = coordinates.Col + 1;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row - 1;
                col = coordinates.Col + 1;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row - 1;
                col = coordinates.Col;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row - 1;
                col = coordinates.Col - 1;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row - 1;
                col = coordinates.Col - 1;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
                
                row = coordinates.Row + 1;
                col = coordinates.Col + 1;
                
                if (_map[row][col] != Strings.Empty) {
                    return _map[row][col];
                }
            }
            
            return "";
        }
        
        // Gets the number of targets near the player
        // A "target" is defined as anything that is not empty space
        public int GetNumNearTargets() {
            var numTargets = 0;
            Coordinates coordinates = GetCoordinates(Strings.PlayerChar);

            if (coordinates != null) {
                if (_map[coordinates.Row + 1][coordinates.Col] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row + 1][coordinates.Col + 1] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row][coordinates.Col + 1] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row - 1][coordinates.Col + 1] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row - 1][coordinates.Col] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row - 1][coordinates.Col - 1] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row - 1][coordinates.Col - 1] == Strings.Empty) {
                    numTargets++;
                }
                if (_map[coordinates.Row + 1][coordinates.Col + 1] == Strings.Empty) {
                    numTargets++;
                }
            }
            
            return numTargets;
        }

        internal string GetMapStr() {
            string map = "";

            foreach (var row in _map) {
                foreach (var col in row) {
                    map += col == Strings.Empty ? " " : col;
                }

                map += "\n";
            }

            return map;
        }

        internal bool MovePlayer(string direction) {
            var playerCoordinates = GetCoordinates(Strings.PlayerChar);
            var oldPlayerCoordinates = new Coordinates(playerCoordinates.Row, playerCoordinates.Col);

            if (direction == Strings.Up) {
                playerCoordinates.Row -= 1;
            }
            else if (direction == Strings.Down) {
                playerCoordinates.Row += 1;
            }
            else if (direction == Strings.Left) {
                playerCoordinates.Col -= 1;
            }
            else if (direction == Strings.Right) {
                playerCoordinates.Col += 1;
            }

            if (playerCoordinates.Row < 0) {
                playerCoordinates.Row = 0;
            }
            if (playerCoordinates.Col < 0) {
                playerCoordinates.Col = 0;
            }
            var maxRow = _map.Count - 1;
            if (playerCoordinates.Row > maxRow) {
                playerCoordinates.Row = maxRow;
            }
            var maxCol = _map[playerCoordinates.Row].Count - 1;
            if (playerCoordinates.Col > maxCol) {
                playerCoordinates.Col = maxCol;
            }

            if (_map[playerCoordinates.Row][playerCoordinates.Col] != Strings.Empty) {
                return false;
            }

            // Replace the space where the player was previously
            Console.SetCursorPosition(oldPlayerCoordinates.Col, oldPlayerCoordinates.Row);
            Console.Write(Strings.EmptyChar);
            _map[oldPlayerCoordinates.Row][oldPlayerCoordinates.Col] = Strings.Empty;
            
            // Draw the player
            Console.SetCursorPosition(playerCoordinates.Col, playerCoordinates.Row);
            Console.Write(Strings.PlayerChar);
            _map[playerCoordinates.Row][playerCoordinates.Col] = Strings.PlayerChar;

            // After updating the map, place the cursor where messages should be displayed
            Interface.SetCursorPosition(this);

            return true;
        }
    }
}