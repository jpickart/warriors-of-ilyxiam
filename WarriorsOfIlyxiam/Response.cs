﻿using System;

namespace WarriorsOfIlyxiam {
    public class Response {
        public Response() { }
        public Response(ConsoleKeyInfo consoleKeyInfo) {
            if (consoleKeyInfo.Key == ConsoleKey.Q) {
                Main = Strings.StopMoving;
            }
            else if (consoleKeyInfo.Key == ConsoleKey.UpArrow) {
                Main = Strings.Move;
                Secondary = Strings.Up;
            }
            else if (consoleKeyInfo.Key == ConsoleKey.DownArrow) {
                Main = Strings.Move;
                Secondary = Strings.Down;
            }
            else if (consoleKeyInfo.Key == ConsoleKey.LeftArrow) {
                Main = Strings.Move;
                Secondary = Strings.Left;
            }
            else if (consoleKeyInfo.Key == ConsoleKey.RightArrow) {
                Main = Strings.Move;
                Secondary = Strings.Right;
            }
            else {
                Main = consoleKeyInfo.KeyChar.ToString();
            }
        }

        public string Main { get; internal set; }
        public string Secondary { get; internal set; }

        internal bool HasTarget() {
            return Secondary != "" && Secondary != null;
        }
    }
}