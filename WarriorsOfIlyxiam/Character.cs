﻿namespace WarriorsOfIlyxiam {
    internal class Character {
        public string charName;
        public int hitPoints;
        private int strength;
        private Weapon weapon;
        public bool isHostile;
        public string MapChar { get; internal set; }

        public Character(string charName) {
            this.charName = charName;

            this.strength = 1;
            this.weapon = null;
        }
    }
}