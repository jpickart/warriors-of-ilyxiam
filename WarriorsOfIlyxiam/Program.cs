﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarriorsOfIlyxiam {
    class Program {
        // Stores all the NPCs in the game
        // The key is the "Char", in other words, the symbol that shows up on the map
        // The value is the NPC object itself
        private static Dictionary<string, NPC> npcs;
        
        // Indicates whether or not the game has been won or not
        private static bool gameWon;

        // The game map
        private static Map map;

        // The player character
        private static Character character;
    
        static void Main(string[] args) {
            Response response = Interface.Prompt(Strings.EnterName);
            npcs = new Dictionary<string, NPC>();

            character = new Character(response.Main);
            character.MapChar = Strings.PlayerChar;
            character.hitPoints = 10;

            var quickMove = true;

            map = new Map("map.txt");

            if (map.IsValid == false) {
                Interface.ShowError(Strings.InvalidMap);
                return;
            }

            Interface.Clear();
            Interface.ShowMap(map.GetMapStr());
            Interface.WriteMessage(Strings.Welcome, map, character.charName);
            
            AddNPCs();

            while (response.Main != Strings.Quit) {
                foreach (KeyValuePair<string, NPC> npcDefinition in npcs) {
                    if (npcDefinition.Value.isHostile == true) {
                        if (map.IsTargetNear(npcDefinition.Value.MapChar, character.MapChar)) {
                            AttackPlayer(npcDefinition.Value);
                        }
                    }
                }
                
                if (quickMove == false) {
                    Console.CursorVisible = true;
                    response = Interface.Prompt(Strings.EnterCommand);
                }
                else {
                    Console.CursorVisible = false;
                    response = new Response(Console.ReadKey());
                }

                if (response.Main == Strings.Help || response.Main == Strings.QuickHelp) {
                    Interface.WriteHelp(map);
                }
                else if (response.Main == Strings.QuickTalk) {
                    var numTargets = map.GetNumNearTargets();
                    
                    if (numTargets > 1) {
                        Interface.WriteMessage(Strings.EnterTalkTarget, map);
                        var target = GetTarget();
                        var isNear = map.IsTargetNear(character.MapChar, target);

                        if (isNear) {
                            ShowConversationResponse(target);
                        }
                        else {
                            Interface.WriteError(Strings.NotNear, map);
                        }
                    }
                    else if (numTargets == 1) {
                        ShowConversationResponse(map.GetNextTarget());
                    }
                    else {
                        Interface.WriteError(Strings.NeedTarget, map);
                    }
                }
                else if (response.Main == Strings.Talk) {
                    if (response.HasTarget()) {
                        var target = Strings.Translate(response.Secondary);
                        // Check the surroundings for the target
                        var isNear = map.IsTargetNear(character.MapChar, target);

                        if (isNear) {
                            ShowConversationResponse(target);
                        }
                        else {
                            Interface.WriteError(Strings.NotNear, map);
                        }
                    }
                    else {
                        Interface.WriteError(Strings.NeedTarget, map);
                    }
                }
                else if (response.Main == Strings.IsNear) {
                    if (response.HasTarget()) {
                        var isNear = map.IsTargetNear(character.MapChar, Strings.Translate(response.Secondary));

                        if (isNear) {
                            Interface.WriteMessage(Strings.Yes, map);
                        }
                        else {
                            Interface.WriteMessage(Strings.No, map);
                        }
                    }
                    else {
                        Interface.WriteError(Strings.NeedTarget, map);
                    }
                }
                else if (response.Main == Strings.ShowMap) {
                    Interface.WriteMessage(map.GetMapStr(), map);
                }
                else if (response.Main == Strings.Move) {
                    if (response.HasTarget()) {
                        var moved = map.MovePlayer(response.Secondary);

                        if (!moved) {
                            Interface.WriteError(Strings.CannotMove, map);
                        }
                        else {
                            Interface.ClearMessages(map);
                        }
                        
                    }
                    else {
                        Interface.WriteError(Strings.NeedTarget, map);
                    }
                }
                else if (response.Main == Strings.QuickMove) {
                    quickMove = true;
                }
                else if (response.Main == Strings.StopMoving) {
                    quickMove = false;
                }
                else if (response.Main == Strings.QuickAttack) {
                    var numTargets = map.GetNumNearTargets();
                    
                    if (numTargets > 1) {
                        Interface.WriteMessage(Strings.EnterAttackTarget, map);
                        var target = GetTarget();
                        var isNear = map.IsTargetNear(character.MapChar, target);

                        if (isNear) {
                            Attack(target);
                        }
                        else {
                            Interface.WriteError(Strings.NotNear, map);
                        }
                    }
                    else if (numTargets == 1) {
                        Attack(map.GetNextTarget());
                    }
                    else {
                        Interface.WriteError(Strings.NeedTarget, map);
                    }
                }
                else {
                    Interface.WriteHelp(map);
                }
            }
        }

        private static string GetTarget() {
            return Console.ReadKey().KeyChar.ToString();
        }

        private static void AddNPCs() {
            var king = new NPC(Strings.KingName, 10);

            king.Nickname = Strings.King;
            king.MapChar = Strings.KingChar;
            king.AddResponse(Strings.Default, Strings.KingDefaultResp);
            king.AddResponse(Strings.GameWon, Strings.KingGameWonResp);
            
            npcs.Add(king.MapChar, king);
            
            var dragon = new NPC(Strings.DragonName, 20);
            
            dragon.Nickname = Strings.Dragon;
            dragon.MapChar = Strings.DragonChar;
            dragon.AddResponse(Strings.Default, Strings.DragonDefaultRep);
            
            npcs.Add(dragon.MapChar, dragon);
        }
        
        // Gets an NPC
        // Returns null if the NPC is not found
        private static NPC GetNPC(string target) {
            if (npcs.ContainsKey(target) == true) {
                return npcs[target];
            }
            else {
                return null;
            }
        }

        private static void AttackPlayer(NPC npc) {
            var damage = 5;
            var charCoordinates = map.GetCoordinates(character.MapChar);

            character.hitPoints -= damage;

            if (character.hitPoints > 0) {
                Interface.WriteMessage(String.Format(Strings.DamageDealtPlayer, npc.charName, damage), map);
            }
            else {
                gameWon = false;
                Interface.WriteError(Strings.PlayerKilled, map);
            }

            Interface.RefreshChar(character, charCoordinates);
        }

        // Initiates a player attack against the target
        private static void Attack(string target) {
            var npc = GetNPC(target);
            
            if (npc == null) {
                return;
            }
            
            var damage = 1;
            
            npc.hitPoints -= damage;
            npc.isHostile = true;

            var charCoordinates = map.GetCoordinates(npc.MapChar);

            if (npc.hitPoints > 0) {
                Interface.WriteMessage(String.Format(Strings.DamageDealt, damage, damage == 1 ? "" : "s", npc.charName), map);
            }
            else {
                Interface.WriteMessage(Strings.TargetKilled, map, npc.charName);
                map.RemoveTarget(target);
                
                if (target == Strings.DragonChar) {
                    gameWon = true;
                }
            }

            Interface.RefreshChar(npc, charCoordinates);
        }
		
        // Shows a conversation response from an NPC
		private static void ShowConversationResponse(string target) {
            var npc = GetNPC(target);
            
            if (npc == null) {
                return;
            }
            
            var response = npc.GetResponse(gameWon == true ? Strings.GameWon : Strings.Default);
            
            if (response != "") {
                Interface.WriteMessage(response, map);
            }
            else {
                Interface.WriteMessage(npc.charName + " has nothing to say to you.", map);
            }
        }
		
    }
}
