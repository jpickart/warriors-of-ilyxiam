﻿using System;
using System.Collections.Generic;

namespace WarriorsOfIlyxiam {
    internal class NPC : Character {
        public string Nickname;
        /// <summary>
        /// A responses dictionary for dialog.
        /// The key is the condition for the response.
        /// The value is the response.
        /// </summary>
        private Dictionary<string, string> _responses;
        public NPC(string charName, int hitPoints) : base(charName) {
            _responses = new Dictionary<string, string>();
            this.hitPoints = hitPoints;
        }

        public void AddResponse(string condition, string response) {
            if (_responses.ContainsKey(condition) == false) {
                _responses[condition] = response;
            }
            else {
                _responses.Add(condition, response);
            }
        }

        public string GetResponse(string condition) {
            string response = "";

            if (_responses.ContainsKey(condition) == true) {
                response = _responses[condition];
            }

            return response;
        }
    }
}