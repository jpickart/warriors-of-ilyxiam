﻿namespace WarriorsOfIlyxiam {
    internal class Strings {
        internal static string Welcome = "Welcome {0}. You are in the throne room of the king with other warriors. \"Go forth and slay the dragon!\" he commands.";
        internal static string Empty = ".";
        internal static string InvalidMap = "Map is invalid, unable to continue. Check the map definition in 'Map' folder";
        
        // Game statuses
        internal static string Default = "default";
        internal static string GameWon = "gameWon";
        
        // Names
        internal static string KingName = "Aluvian";
        internal static string DragonName = "Xylarm";
        
        // Nicknames
        internal static string King = "king";
        internal static string Dragon = "dragon";
        
        // Dialog responses
        internal static string KingDefaultResp = "What are you waiting for? The dragon is going to burst in here at any minute!";
        internal static string KingGameWonResp = "Well done!";
        internal static string DragonDefaultRep = "If you value your life, I suggest you flee.";
        
        // Prompts
        internal static string EnterName = "Enter your character's name";
        internal static string EnterCommand = "Enter command";
        internal static string EnterTalkTarget = "Who do you want to talk to?";
        internal static string EnterAttackTarget = "Who do you want to attack?";
        
        // Game map characters
        internal static string PlayerChar = "@";
        internal static string KingChar = "k";
        internal static string DragonChar = "d";
        internal static string EmptyChar = " ";
        internal static string WallChar = "x";
        
        // Interface feedback
        internal static string CannotMove = "You cannot move in that direction";
        internal static string NotNear = "Target is not close enough or does not exist";
        internal static string NeedTarget = "You must specify a target to use with this command.";
        internal static string Yes = "Yes";
        internal static string No = "No";
        internal static string DamageDealt = "You do {0} point{1} of damage to {2}";
        internal static string DamageDealtPlayer = "{0} attacked you and dealt {1} points of damage!";
        internal static string TargetKilled = "You kill {0}!";
        internal static string PlayerKilled = "You were killed!";

        // Directions
        internal static string Up = "up";
        internal static string Down = "down";
        internal static string Left = "left";
        internal static string Right = "right";
        
        // "Long" commands
        internal static string Help = "help";
        internal static string Talk = "talk";
        internal static string IsNear = "near";
        internal static string Quit = "quit";
        internal static string ShowMap = "map";
        internal static string QuickMove = "quick";
        internal static string Move = "move";
        internal static string StopMoving = "stop";
        
        // Single key commands
        internal static string QuickTalk = "t";
        internal static string QuickAttack = "a";
        internal static string QuickHelp = "h";

        // Translates the passed string to its equivalent game map character
        internal static string Translate(string key) {
            if (key == "" || key == null) {
                return key;
            }

            string tempVal = key.ToLower();

            if (tempVal == Strings.King) {
                return Strings.KingChar;
            }

            return key;
        }
    }
}