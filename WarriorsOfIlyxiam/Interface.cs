﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarriorsOfIlyxiam {
    public class Interface {
        private static string _help = "Commands: (q)uit, (h)elp, (t)alk, (n)ear, (m)ap";
        private static int _maxMessages = 3;

        /// <summary>
        /// Keeps track of the current messages that are displayed.
        /// </summary>
        private static List<string> _messages = new List<string>();

        public static Response Prompt(string str) {
            Response response = new Response();
            string input;
            string[] splitInput;

            Console.Write(str + ": ");

            input = Console.ReadLine();
            splitInput = input.Split(' ');

            response.Main = splitInput[0].ToLower();

            if (splitInput.Length > 1) {
                response.Secondary = splitInput[1];    
            }

            return response;
        }

        internal static void ShowMap(string map) {
            Console.WriteLine(map);
        }

        internal static void ClearMessages(Map map) {
            int counter = 0;

            foreach (var message in _messages) {
                string buffer = "";
                
                for (int i = 0; i < message.Length; i++) {
                    buffer += " ";
                }

                SetCursorPosition(map, counter);
                Console.Write(buffer);

                counter++;
            }
            _messages = new List<string>();
        }

        private static void UpdateCursorPosition(Map map) {
            SetCursorPosition(map, _messages.Count);
        }

        private static void AddMessage(string message, Map map) {
            _messages.Add(message);

            if (_messages.Count >= _maxMessages) {
                ClearMessages(map);
            }
        }

        internal static void WriteHelp(Map map) {
            AddMessage(_help, map);
            WriteMessage(_help, map);
        }

        internal static void WriteMessage(string message, Map map, string param = "") {
            AddMessage(message, map);
            UpdateCursorPosition(map);
            Console.Write(message, param);
        }

        internal static void WriteError(string message, Map map, string param = "") {
            AddMessage(message, map);
            UpdateCursorPosition(map);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(message, param);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        internal static void ShowError(string message, string param = "") {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message, param);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        internal static void Clear() {
            Console.Clear();
        }

        internal static void SetCursorPosition(Map map, int rowOffset = 0) {
            Console.SetCursorPosition(map.GetMessageCol(), map.GetMessageRow() + rowOffset);
        }

        internal static void RefreshChar(Character character, Coordinates coordinates) {
            Console.SetCursorPosition(coordinates.Col, coordinates.Row);

            string tempChar = character.MapChar;

            if (character.hitPoints <= 0) {
                tempChar = Strings.EmptyChar;
            }
            if (character.isHostile == true) {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            
            Console.Write(tempChar);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
